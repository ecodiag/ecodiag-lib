#!/usr/bin/env bash

#
# Rationale: 
#   - reformat(prettier) and lint(eslint) our files
#   - used by pre-commit to run before committing
npx prettier --no-semi --single-quote --trailing-comma none  --write "$@"  && npm run lint $@
