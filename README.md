# ecodiag-lib

## Develop locally

- Assuming `ecodiag-lib` is cloned under `/workspace/ecodiag-lib`
- Assuming your client projet is located in `/workspace/client-code`

You can develop in parallel your 2 codes by "linking" ecodiag-lib to your client code:

```bash
$) cd /workspace/ecodiag-lib
$) npm link
$) cd /workspace/client-code
$) npm link ecodiag-lib

# from this point any changes in ecodiag-lib are visible in the ecodiag-lib dependency in the client-code.

$) ls -ld node_modules/ecodiag-lib

# You'll need to transpile the code everytime in the client code to make
# it available for es6 projects 
$) cd /workspace/ecodiag-lib
# make change to ecodiag-lib + rebuild
$) npm run build
```

Réference: https://docs.npmjs.com/cli/v6/commands/npm-link

## Code maintenance

### Format lint

We have a minimal configuration for [pre-commit](https://pre-commit.com/) (that
runs git pre commits automatically).

```
pip install pre-commit

# install the pre commit hook (opt-in)
pre-commit --install
```


### Release


#### Automatic publication

- Change package version in  `package.json`
- Tag accordingly
- Push the tags, the package will be published by Gitlab CI.


#### Manual publication

```
# transpile
npm run build
# publish
NPM_TOKEN=XXX npm publish
```

-> need a token with API access to push on the gitlab registry
