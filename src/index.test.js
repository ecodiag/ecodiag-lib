import { Device } from '.'

// https://jestjs.io/docs/api#2-testeachtablename-fn-timeout
describe('fromRegex', () => {
  test.each`
    type          | model               | eType       | eModel              | eScore
    ${'Portable'} | ${'MacBook Pro 15'} | ${'laptop'} | ${'macbook_pro_15'} | ${2}
    ${'Portable'} | ${'Latitude 7480'}  | ${'laptop'} | ${'latitude_7xxx'}  | ${2}
    ${'Portable'} | ${'XPS 9365'}       | ${'laptop'} | ${'default'}        | ${1}
    ${'Portable'} | ${'gibberish'}      | ${'laptop'} | ${'default'}        | ${1}
    ${'Laptop'}   | ${'gibberish'}      | ${'laptop'} | ${'default'}        | ${1}
  `(
    'can create device from ${type} and ${model}',
    ({ type, model, eType, eModel, eScore }) => {
      let d = Device.fromRegex(type, model)
      expect(d.type).toBe(eType)
      expect(d.model).toBe(eModel)
      expect(d.score).toBe(eScore)
    }
  )

  test('Cannot create Device from gibberish type', () => {
    let throwFromRegexp = function () {
      return Device.fromRegex('gibberish', 'gibberish')
    }
    expect(throwFromRegexp).toThrow()
  })
})

describe('i18n', () => {
  test('i18nType found', () => {
    let d = Device.fromRegex('Portable', 'MacBook Pro 15')
    expect(d.i18nType('fr')).toBe('PC portable')
    expect(d.i18nType('en')).toBe('Laptop')
  })

  test('i18nType not found', () => {
    let d = new Device()
    expect(d.i18nType('fr')).toBeUndefined()
  })

  test('i18nModel not found', () => {
    let d = new Device()
    expect(d.i18nType('fr')).toBeUndefined()
  })

  test('i18nModel default', () => {
    let d = new Device()
    // this sets the default model  using PARAMS.get_default_model
    d.type = 'screen'
    expect(d.i18nModel('fr')).toBe('__default__')

    d = new Device()
    d.type = 'desktop'
    // NOTE: ecodiag_avg_pc is the default model (see PARAMS)
    expect(d.i18nModel('fr')).toBe('PC fixe (moy.)')
    expect(d.i18nModel('en')).toBe('Tower (average)')
  })

  test('i18nModel', () => {
    let d = Device.fromRegex('Portable', 'MacBook Pro 15')
    expect(d.i18nModel('fr')).toBe('MacBook pro 15/16"')
    expect(d.i18nModel('en')).toBe('MacBook pro 15/16"')
  })
})
