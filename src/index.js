import { devices, i18nModel, i18nType } from './devices.js'
import { v4 as uuidv4 } from 'uuid'

// reexport devices and messages
export { devices, i18nModel, i18nType } from './devices.js'

// PARAMS isn't a constant !
// this can be changed by user code (eg l1p5 Main.vue)
export let PARAMS = {
  includes_empty_year: false, // used in the "flux" method
  damping_factor: 1, // in years, used in the "flux" method
  ignore_year: false, // used in the "flux" method
  lifetime_factor: 1.5, // used in the "flux" method to simulate a longer lifetime, or in the "stock" method to compute the default extended lifetime
  default_uncertainty: 30,
  lifetime_uncertainty: 1,
  kWh_to_CO2e: 0.084,
  get_default_model: get_default_model_ecodiag
}

export const STATUS = {
  user_ko: 2,
  user_ok: 4,
  unknown: 6,
  unknown_year: 7,
  invalid_year: 8,
  csv_ok: 10
}

/**
 * Setup the library according to options
 *
 * @param {Object} params - the params to set
 */
export function setup(params) {
  for (let [k, v] of Object.entries(params)) {
    PARAMS[k] = v
  }
}

export function is_empty_year(y) {
  return !y || y === ''
}

export function is_valid_year(y, method, ref_year) {
  return (
    method === 'stock' ||
    PARAMS.ignore_year ||
    (PARAMS.includes_empty_year && is_empty_year(y)) ||
    (y <= ref_year && y > ref_year - PARAMS.damping_factor)
  )
}

export function year_ok(y, method, referenceYear) {
  return is_valid_year(y, method, referenceYear)
}

export function compute_status(item, method, ref_year) {
  let check_year = function (ok) {
    return !is_valid_year(item.year, method, ref_year)
      ? is_empty_year(item.year)
        ? STATUS.unknown_year
        : STATUS.invalid_year
      : ok
  }
  if (item.details.filter((e) => Boolean(e.csvdata)).length === 0) {
    return Object.keys(devices).includes(item._type)
      ? check_year(STATUS.user_ok)
      : STATUS.user_ko
  } else {
    return item.score < 1 ? STATUS.unknown : check_year(STATUS.csv_ok)
  }
}

export function is_valid(item, method, ref_year) {
  let status = compute_status(item, method, ref_year)
  return (
    item.score >= 0 && (status === STATUS.user_ok || status === STATUS.csv_ok)
  )
}

export function get_default_model(type) {
  return PARAMS.get_default_model(type)
}

function my_erfinv(x) {
  var z = 0
  var a = 0.1400122886866665 // = ((8*(Math.PI - 3)) / ((3*Math.PI)*(4 - Math.PI)))
  var sign_x
  if (x == 0) {
    sign_x = 0
  } else if (x > 0) {
    sign_x = 1
  } else {
    sign_x = -1
  }

  if (x != 0) {
    var tmp1 = Math.log(1 - x * x)
    var tmp2 = tmp1 / 2 + 2 / (Math.PI * a)
    z = Math.sqrt(Math.sqrt(tmp2 * tmp2 - tmp1 / a) - tmp2) * sign_x
  }
  return z
}

function LogNormal(mu, sigma) {
  // this.mu = mu
  // this.sigma = sigma

  let pdf = function (x) {
    return (
      Math.exp(-Math.pow(Math.log(x) - mu, 2) / (2 * sigma * sigma)) /
      (x * sigma * Math.sqrt(2 * Math.pi))
    )
  }

  let cdf = function (x) {
    return 0.5 * 0.5 * Math.erf((Math.log(x) - mu) / (Math.sqrt(2) * sigma))
  }

  let inv = function (p) {
    return Math.exp(mu + Math.sqrt(2 * sigma * sigma) * my_erfinv(2 * p - 1))
  }

  return { pdf, cdf, inv }
}

function LogNormalFromMeanAndRelStdev(mean, relative_stdev) {
  var si2 = Math.log(Math.pow(relative_stdev, 2) + 1)
  return LogNormal(Math.log(mean) - 0.5 * si2, Math.sqrt(si2))
}

// Returns the attribute "attr" from a type/model pair
// (model is optional)
export function get_device_attribute(type, model, attr) {
  var res = undefined
  var device = devices[type]
  if (device) {
    res = device[attr]
    if (
      model &&
      model != 'default' &&
      devices[type].models &&
      model in devices[type].models &&
      devices[type].models[model][attr]
    ) {
      res = devices[type].models[model][attr]
    }
  }
  return res
}

function get_default_model_ecodiag(type) {
  if (devices[type] && devices[type].models) {
    var res = {
      desktop: 'ecodiag_avg_PC',
      printer: 'office_40_99kg'
    }[type]
    if (res) {
      return res
    }
    return 'default'
  } else {
    return undefined
  }
}
// Returns grey CO2 factor from a type/model pair
// (model is optional)
export function get_device_factor(type, model) {
  function getmean(el) {
    if (el.mean) {
      return el.mean
    } else {
      return el
    }
  }
  let getstd = function (el) {
    if (el.std) {
      return el.std
    } else {
      return PARAMS.default_uncertainty / 100
    } // default is 30%
  }

  if (!(type in devices)) {
    return { mean: 0, std: 0 }
  }

  let device_CO2 = devices[type].grey_CO2
  let mean = getmean(device_CO2)
  let std = getstd(device_CO2)
  if (model && model != 'default') {
    let model_CO2
    if (devices[type].models[model].grey_CO2) {
      model_CO2 = devices[type].models[model].grey_CO2
    } else {
      model_CO2 = devices[type].models[model]
    }
    mean = getmean(model_CO2)
    std = getstd(model_CO2)
  }
  return { mean: mean, std: std }
}

export class Device {
  constructor() {
    this.id = uuidv4()
    this._type = null
    this._model = null
    this.nb = 1
    this.lifetime = null
    this.lifetime2 = null
    this.year = null
    this.score = 0
    this.details = []
    this.tag = ''
  }

  get type() {
    return this._type
  }

  set type(v) {
    if (this._type != v) {
      this._type = v
      this.model = get_default_model(this._type)
      this.update_from_type_and_model()
    }
  }

  get model() {
    return this._model
  }

  set model(v) {
    if (this._model != v) {
      this._model = v
      this.update_from_type_and_model()
    }
    // what else ?
  }

  // Shortcut to get_device_attribute (obsolete)
  get get_yearly_consumption() {
    return get_device_attribute(this.type, this.model, 'yearly_consumption')
  }

  update_from_type_and_model() {
    this.lifetime = get_device_attribute(this.type, this.model, 'duration')
    this.lifetime2 = this.lifetime * +PARAMS.lifetime_factor
    this.yearly_consumption = get_device_attribute(
      this.type,
      this.model,
      'yearly_consumption'
    )
    this.usage = get_device_attribute(this.type, this.model, 'usage')
  }

  static fromType(type, params) {
    if (!params) params = {}

    let item = new Device()
    item.type = type // this automatically sets model, lifetime, lifetime2, yearly_consumption, and usage from default values
    // now, let's update the model (this also sets lifetime, lifetime2, yearly_consumption, and usage from default values)
    if (
      (params.model &&
        type in devices &&
        devices[type].models &&
        params.model in devices[type].models) ||
      params.model == 'default'
    ) {
      item.model = params.model
    }
    // now, let's update them if provided by params,
    // that is, copy all parameters but 'type' and 'model':
    for (let key in params) {
      if (
        key !== 'type' &&
        key !== 'model' &&
        key !== '_type' &&
        key !== '_model'
      ) {
        const val = params[key]
        item[key] = val
      }
    }

    return item
  }

  /**
   *
   * Tries to find a good candidate in device based on the given string
   *
   * @param {String} type - string to match to the regexp type in devices
   * @param {String} model - string to match to the regexp model in devices
   *
   * @returns {Device} device - the matched Device
   */
  static fromRegex(type, model) {
    let score = 0

    let item = {}
    item['score'] = 0
    item['type'] = ''
    item['model'] = ''

    for (let dev_key in devices) {
      const dev = devices[dev_key]
      let dev_score = 0
      let mod_score = 0
      if (dev.regex && type) {
        if (type.search(dev.regex) >= 0) {
          dev_score = 1
        }
      }
      if (dev_score > 0 && score == 0) {
        // we already found a better match
        score = dev_score
        item['score'] = score
        item['type'] = dev_key
        item['model'] = get_default_model(dev_key)
      }

      if (dev.models && model) {
        for (var m_key in dev.models) {
          const m = dev.models[m_key]
          if (m.regex) {
            if (model.search(m.regex) >= 0) {
              mod_score = 1
            }
            if (dev_score + mod_score > score) {
              // we found a better candidate
              score = dev_score + mod_score
              item['score'] = score
              item['type'] = dev_key
              item['model'] = m_key
              if (score == 2) {
                return Device.fromType(item['type'], item)
              } else {
                break
              }
            }
          }
        }
      }
    }

    // after a lookup we return the best candidate so far ...
    if (score > 0) {
      return Device.fromType(item['type'], item)
    }
    // if no good candidate, throw
    throw new Error(`No good enough device found for ${type} and ${model}"`)
  }

  // Computes the estimated CO2e emissions of the input item,
  // as well as the inferior/superior bounds for the provided confidences (optional)
  // - method should be either 'stock' or 'flux'
  compute_device_co2e(method, confidences) {
    const device_factor = get_device_factor(this.type, this.model)
    const lt = method == 'stock' ? this.lifetime : +PARAMS.damping_factor
    const greyCO2 = (this.nb * device_factor.mean) / lt

    const lt_bis =
      method == 'stock'
        ? this.lifetime2
        : +PARAMS.damping_factor * +PARAMS.lifetime_factor
    const greyCO2_bis = (this.nb * device_factor.mean) / lt_bis

    let infs = []
    let sups = []
    if (confidences) {
      const distrib = LogNormalFromMeanAndRelStdev(
        device_factor.mean,
        device_factor.std
      )

      infs = Array(confidences.length)
      sups = Array(confidences.length)

      for (var i = 0; i < confidences.length; ++i) {
        infs[i] =
          (this.nb * distrib.inv(0.5 - (0.5 * confidences[i]) / 100)) / lt
        sups[i] =
          (this.nb * distrib.inv(0.5 + (0.5 * confidences[i]) / 100)) / lt
      }
    }
    const useCO2 = this.nb * this.get_yearly_consumption * PARAMS.kWh_to_CO2e

    return {
      grey: greyCO2, // mean
      infs: infs,
      sups: sups,

      use: useCO2,

      grey2: greyCO2_bis // reduction objective
    }
  }

  /**
   * Try to find a translation for the device in devices
   *
   * @param {*} locale - locale to look for (e.g fr, en, ...)
   *
   * @returns {?string} the translation if any
   */
  i18nType(locale) {
    return i18nType(this.type, locale)
  }

  /**
   * Try to find a translation for the model in devices
   *
   * @param {*} locale - locale to look for (e.g fr, en, ...)
   *
   * @returns {?string} - the translation if any
   */
  i18nModel(locale) {
    return i18nModel(this.type, this.model, locale)
  }
}

// make some getter enumerable (backward compat)
Object.defineProperty(Device.prototype, 'type', { enumerable: true })
Object.defineProperty(Device.prototype, 'model', { enumerable: true })
